
# coding: utf-8

# In[1]:


from zero import Circuit
from zero.components import Resistor, Capacitor
import numpy as np


# ### Helper Function

# In[2]:


def createNLCompFunc(name,node1,node2,comType):
    if comType is "capacitor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_capacitor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    elif comType is "resistor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_resistor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    elif comType is "inductor":
        def NLcomp(circuit,present=False, value="0"):
            if present:
                if circuit.has_component(name):
                    circuit.remove_component(name)
                circuit.add_inductor(name=name,value=value, node1=node1, node2=node2)
            elif circuit.has_component(name):
                circuit.remove_component(name)
    else:
        print("Error:")
        print("Use comType capacitor , resistor or inductor")
    return NLcomp

# Function to list all components connected to some particular node
from zero.components import Node
def cmpConnTo(nodeName):
    nx = Node(nodeName)
    for cmp in circuit.components:
        if nx in cmp.nodes:
            print(cmp)


# ***
# ***
# # Circuit Definition
# Following are some useful node/component names to remember:
# 
# |	Node name	|	Function/Parameter	|
# |	---	|	---	|
# |<img width=100/>| <img width=600/> |
# |	rfbnIN	|	Input to FSS after mixer	|
# |	pztnOUT	|	Output from PZT board	|
# |	eomhvnOUT	|	Output from EOM HV Board	|
# |   comnMIXR   |   MIXR channel port   |
# |   eomnPCMon   |   PC Mon port   |
# |   pztnFASTMON |   FAST Mon port |
# |	comOUT1	|	Input impedance of measuring instrument connected to OUT1 at common path.<br>Default is a resistance of 50 Ohm.	|
# |	comOUT2	|	Input impedance of measuring instrument connected to OUT2 at common path.<br>Default is a resistance of 50 Ohm.	|
# |	pztOUT1	|	Input impedance of measuring instrument connected to OUT1 at pzt path.<br>Default is a resistance of 50 Ohm.	|
# |	pztOUT2	|	Input impedance of measuring instrument connected to OUT2 at pztpath.<br>Default is a resistance of 50 Ohm.	|
# |	pztCLaser	|	Capacitance of Laser PZT. <br>Added when LaserConnected(True) is used.	|
# |	eomhvCEOM	|	Capacitance of EOM. <br>Added when EOMConnected(True) is used.	|
# 
# Test ports can be enabled by the corresponding test switches:
# 
# |	Test Switch Function	|	Test EXC Node	|	Test OUT1 Node	|	Test OUT2 Node	|
# |	---	|	---	|	---	|	---	|
# |	test1SW(circuit, state)	|	Test1ExcnIN	|	-	|	-	|
# |	test2SW(circuit, state)	|	Test2ExcnIN	|	comnOUT1	|	comnOUT2	|
# |	test3SW(circuit, state)	|	Test3ExcnIN	|	pztnOUT1	|	pztnOUT2	|
# 
# Additional functions:
# 
# |	Function	|	What does it do?	|	Arguments and usage	|
# |	---	|	---	|	---	|
# |<img width=400/>|<img width=300/>|<img width=500/>|
# |	COMGain(circuit,G=None,Vg=None,vb=True)	|	Set common gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
# |	FASTGain(circuit,G=None,Vg=None,vb=True)	|	Set Fast gain	|	circuit: Circuit object <br>G: Gain in dB<br>Vg:Gain setting voltage<br>vb: verbose<br>One only needs to provide one of the arguments G or Vg	|
# |	PZTBoost(circuit,state)	|	Switch on or off PZT Boost. Equivalent to the side switch on TTFSS box.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
# |	PZTSign(circuit,sign)	|	Sign selection switch same as the front panel sign selection switch in TTFSS box.	|	circuit: Circuit object <br>State: '+' or '_'	|
# |	rampEngage(circuit,state)	|	Engages Ramp Switch on FSS.	|	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
# |	LaserConnected(circuit, connected=True)	|	Function to add or remove the laser at pzt out. This adds or removes the capacitance due to lazer pzt.	|	connected: True or False	|
# |	EOMConnected(circuit, connected=True)	|	Function to add or remove the EOM at HV EOM output. This adds or removes the capacitance due to EOM.	|	connected: True or False	|
# |	xxxYzz(circuit,present=False, value="0")	|	Function to add (or remove) otherwise NL component.<br> xxx: Board name 'com', 'eom' etc<br>Y: 'R' for resistor, 'C' for capacitor, 'L' for inductor<br>zz: Circuit symbol number on schematic<br> Example: comC1()	|	circuit: Circuit object <br>present: True or False<br?value: Value if present.	|
# 
# Nomenclature:
# * All nodes are named as boardname + 'n' + number. Special node names which are defined for easy access are listed above.
# * Boardnames are either 'rfb', 'com', 'pzt', 'eom' or 'eomhv'.
# * All circuit elements are names as boardname + circuitSymbol. Ex: pztR43
# * All clip testpoints are named as boardname + 'tp' + TestPointNumber + 'n'. Example 'rfbtp2n'
# * Connections between different boards are through 0 $\Omega$ resistors named: board1name + 'to' + board2name
# 
# ### Initialize circuit object

# In[3]:


circuit = Circuit()


# ***
# ## [RF Board LIGO-D0901894](https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?.submit=Identifier&docid=D0901894&version=)
# ### Test1 SMA Port
# <img src="images/RFB_TestSMA.png" style="max-height:100px; width:auto;" align="left">

# In[4]:


circuit.add_resistor(name="rfbR10", value="100", node1="rfbn2on", node2="gnd")


# ### Input PD Port After Mixer
# <img src="images/RFB_InputPDAfterMixer.png" style="max-height:100px; width:auto;" align="left">

# In[5]:


circuit.add_resistor(name="rfbR9", value="22", node1="rfbn1", node2="rfbn2off")


# ### Test1 Switch
# <img src="images/RFB_Test1Switch.png" style="max-height:200px; width:auto;" align="left">

# In[6]:


# Input connection O Ohm resistor
circuit.add_resistor(name="rfbInpCon", value="0", node1="rfbnIN", node2="rfbn1")

def test1SW(circuit,state):
    RoffIN = Resistor(name="rfbR9", value="22", node1="rfbn1", node2="rfbn2off") # Temporary measure
    Ron = Resistor(name="Test1SWRon", value="20", node1="rfbn2on", node2="rfbn2")
    Roff = Resistor(name="Test1SWRoff", value="20", node1="rfbn2off", node2="rfbn2")
    if state is 'OFF':
        if circuit.has_component(Ron):
            circuit.remove_component(Ron)
            circuit.add_component(Roff)
            circuit.add_component(RoffIN) # Temporary measure
        else:
            print('Test1 Switch is already OFF.')
        circuit['rfbInpCon'].node2 = "rfbn1"
        print('Input from PD. Input node: rfbnIN')
    elif state is 'ON':
        if circuit.has_component(Roff):
            circuit.remove_component(Roff)
            circuit.remove_component(RoffIN) # Temporary measure
            circuit.add_component(Ron)
        else:
            print('Test1 Switch is already ON.')
        circuit['rfbInpCon'].node2 = "rfbn2on"
        print('Input from Test1 BNC Port. Input node: rfbnIN')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')
    circuit._regenerate_node_set()

# Initialize the circuit in off state (This must be done manually once)
circuit.add_resistor(name="Test1SWRoff", value="20", node1="rfbn2off", node2="rfbn2")
test1SW(circuit,'OFF')


# ### Elliptical Filter
# <img src="images/RFB_EllipticalFilter.png" style="max-height:200px; width:auto;" align="left">

# In[7]:


circuit.add_inductor(name="rfbL3", value="1.2u", node1="rfbn2", node2="rfbn3")
circuit.add_capacitor(name="rfbC12",value="47p", node1="rfbn2", node2="rfbn3")
circuit.add_capacitor(name="rfbC13",value="220p", node1="rfbn2", node2="gnd")
circuit.add_capacitor(name="rfbC14",value="220p", node1="rfbn3", node2="gnd")


# ### List of Clip Testpoints on rfb

# In[8]:


circuit.add_resistor(name="rfbtp2", value="0", node1="rfbn3", node2="rfbntp2")


# ***
# ## [COM Board LIGO-D040105](https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?.submit=Identifier&docid=D040105&version=)
# ### Connection from rfb to com

# In[9]:


circuit.add_resistor(name="rfbtocom", value="0", node1="rfbn3", node2="comn1")


# ### First Amplification and Addition Stage
# <img src="images/COM_FirstStageAmplifierAndAdder.png" style="max-height:300px; width:auto;" align="left">

# In[10]:


# PD Input
circuit.add_resistor(name="comR3",value="124", node1="comn1", node2="comn2")
# AO Input
circuit.add_resistor(name="comR5",value="3.92k", node1="comn8", node2="comn2")
# OS Input
circuit.add_resistor(name="comR9",value="100k", node1="comn6", node2="comn2")
circuit.add_resistor(name="comR8",value="100k", node1="comn5", node2="comn6")
circuit.add_capacitor(name="comC5",value="1u", node1="comn6", node2="gnd")
# Opamp
circuit.add_library_opamp(name="comU3",model="ad829", node1="gnd", node2="comn2", node3="comn3")
circuit.add_resistor(name="comR4",value="392", node1="comn2", node2="comn3")
# NL Capacitor adding function, Default is NL
comC1 = createNLCompFunc(name="comC1", node1 = "comn2", node2 = "comn3", comType = "capacitor")
comC1(circuit,False)
# MIXR Channel
circuit.add_resistor(name="comMIXR",value="0", node1="comn3", node2="comnMIXR")


# ### Test BNC Ports and Test2 Switch
# <img src="images/COM_TestBNCPortsAndTest2Switch.png" style="max-height:300px; width:auto;" align="left">

# In[11]:


# OUT1
circuit.add_resistor(name="comR2",value="453", node1="comn1", node2="comnOUT1")
# OUT2
circuit.add_resistor(name="comR1",value="453", node1="comn3", node2="comnOUT2")
# EXC on COM board, connected by switching on Test2 Switch
circuit.add_resistor(name="comR11",value="1.2k", node1="comn7", node2="comn2")

# Input connection O Ohm resistor
#circuit.add_resistor(name="comTest2Exc", value="0", node1="test2ExcnIN", node2="comn7on")
#circuit.add_resistor(name="comR12",value="49.9", node1="comn7on", node2="gnd")
#circuit.add_resistor(name="comR13",value="1k", node1="comn7off", node2="gnd")

def test2SW(circuit, state, rout1 = "50", rout2 = "50"):
    Test2SWRon = Resistor(name="Test2SWR", value="20", node1="comn7on", node2="comn7")
    Test2SWRoff = Resistor(name="Test2SWR", value="20", node1="comn7off", node2="comn7")
    comTest2Exc = Resistor(name="comTest2Exc", value="0", node1="test2ExcnIN", node2="comn7on")
    comR12 = Resistor(name="comR12",value="49.9", node1="comn7on", node2="gnd")
    comR13 = Resistor(name="comR13",value="1k", node1="comn7off", node2="gnd")
    ONlist = [Test2SWRon, comTest2Exc, comR12]
    OFFlist = [Test2SWRoff, comR13]
    AlwaysCheck = ['comOUT1','comOUT2']
    if rout1 is not "NC":
        ROUT1 = Resistor(name="comOUT1",value=rout1, node1="comnOUT1", node2="gnd") 
        ONlist.append(ROUT1)
    if rout2 is not "NC":
        ROUT2 = Resistor(name="comOUT2",value=rout2, node1="comnOUT2", node2="gnd")
        ONlist.append(ROUT2)
    
    # Remove both list:
    for cmp in ONlist+OFFlist+AlwaysCheck:
        if circuit.has_component(cmp):
            # print('Deleting '+str(cmp))
            circuit.remove_component(cmp)
        
    if state is 'OFF':
        for cmp in OFFlist:
            circuit.add_component(cmp)
            # print('Adding '+str(cmp))
        print('Test2 EXC Not Connected.')
    elif state is 'ON':
        for cmp in ONlist:
            circuit.add_component(cmp)
            # print('Adding '+str(cmp))
        print('Test2 EXC is connected.')
        print('Input node: test2ExcnIN')
        print(rout1+' Ohm and '+rout2+' Ohm impedances connected to OUT1 and OUT2 respectively.')
        print('For different input impedance of measuring instrument, change values of comOUT1 and comOUT2')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')
    circuit._regenerate_node_set()

# Initialize the circuit in off state
test2SW(circuit,'OFF')


# ### Filter to Ramp Engage Switch A
# <img src="images/COM_Passive_Filter.png" style="max-height:150px; width:auto;" align="left">

# In[12]:


circuit.add_resistor(name="comR7",value="100", node1="comn3", node2="comn4on")
# NL Capacitor adding function
comC4 = createNLCompFunc(name = "comC4", node1 = "comn3", node2 = "comn4on", comType = "capacitor")
comC4(circuit,False)


# ### Ramp Engage Switch A
# Adding only the on resistance for now. In a later cell, a function will implement switching.
# 
# <img src="images/COM_RampEngage.png" style="max-height:150px; width:auto;" align="left">

# In[13]:


# Default is ON. The function is added later
circuit.add_resistor(name="rampEngU1ARon", value="20", node1="comn4on", node2="comn4")
circuit.add_resistor(name="comREoffgnd", value="0", node1="comn4off", node2="gnd")


# ### COM Gain
# <img src="images/COM_COMGain.png" style="max-height:200px; width:auto;" align="left">

# In[14]:


circuit.add_library_opamp(name="comU2A", model="ad602", node1="comn4", node2="gnd", node3="comn9")
circuit.add_resistor(name="comU2AinImp", value="100", node1="comn4", node2="gnd")
circuit.add_resistor(name="comR10", value="4.99k", node1="comn9", node2="gnd")
def COMGain(circuit,G=None,Vg=None,vb=True):
    if G is None:
        if Vg is None:
            print('Error: Provide gain in dB or gain setting voltage in V.')
            print('Nothing changed.')
            return 0
        else:
            G = 32.0*Vg + 10.0
    if G>30.0 or G<-10.0:
        print('Error: Gain can be set between -10dB to 30 dB only.')
        print('Nothing changed.')
        return 0
    circuit["comU2A"].a0 = 10**(G/20) #Gain in absolute value
    if vb:
        print('Common Gain set to '+str(np.round(G,2))+' dB.')
#Default set gain to 10 dB
COMGain(circuit,G=10)


# ### List of Clip Testpoints on com

# In[15]:


circuit.add_resistor(name="comtp1", value="0", node1="comn1", node2="comntp1")
circuit.add_resistor(name="comtp2", value="0", node1="comn8", node2="comntp2")
circuit.add_resistor(name="comtp3", value="0", node1="comn6", node2="comntp3")
circuit.add_resistor(name="comtp4", value="0", node1="comn9", node2="comntp4")
circuit.add_resistor(name="comtp5", value="0", node1="comn3", node2="comntp5")


# ***
# ## [PZT Path on Board LIGO-D040105](https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?.submit=Identifier&docid=D040105&version=)
# ### Connection from com to pzt

# In[16]:


circuit.add_resistor(name="comtopzt", value="0", node1="comn9", node2="pztn1")


# ### FAST Gain
# <img src="images/PZT_FASTGain.png" style="max-height:200px; width:auto;" align="left">

# In[17]:


circuit.add_resistor(name="pztR34", value="150", node1="pztn1", node2="pztn2")
circuit.add_library_opamp(name="pztU2B", model="ad602", node1="pztn2", node2="gnd", node3="pztn3")
circuit.add_resistor(name="pztU2BinImp", value="100", node1="pztn2", node2="gnd")
def FASTGain(circuit,G=None,Vg=None,vb=True):
    if G is None:
        if Vg is None:
            print('Error: Provide gain in dB or gain setting voltage in V.')
            print('Nothing changed.')
            return 0
        else:
            G = 32.0*Vg + 10.0
    if G>30.0 or G<-10.0:
        print('Error: Gain can be set between -10dB to 30 dB only.')
        print('Nothing changed.')
        return 0
    circuit["pztU2B"].a0 = 10**(G/20) #Gain in absolute value
    if vb:
        print('Fast Gain set to '+str(np.round(G,2))+' dB.')
#Default set gain to 14 dB
FASTGain(circuit,G=14)


# ### Stage 1
# <img src="images/PZT_Stage1.png" style="max-height:300px; width:auto;" align="left">

# In[18]:


circuit.add_resistor(name="pztR35", value="560", node1="pztn3", node2="pztn4")
circuit.add_library_opamp(name="pztU8", model="op27", node1="pztn5", node2="pztn4", node3="pztn6")
circuit.add_resistor(name="pztR38", value="499", node1="pztn5", node2="gnd")
circuit.add_resistor(name="pztR30", value="4.87k", node1="pztn4", node2="pztn6")
circuit.add_capacitor(name="pztC36", value="3.3n", node1="pztn4", node2="pztn6")


# ### Stage 2
# <img src="images/PZT_Stage2.png" style="max-height:300px; width:auto;" align="left">

# In[19]:


circuit.add_resistor(name="pztR36", value="750", node1="pztn6", node2="pztn7")
circuit.add_library_opamp(name="pztU9", model="ad797", node1="gnd", node2="pztn7", node3="pztn8")
circuit.add_resistor(name="pztR31", value="3.09k", node1="pztn7", node2="pztn8")
circuit.add_capacitor(name="pztC37", value="1.5n", node1="pztn7", node2="pztn8")


# ### Stage 3 and Boost Switch
# <img src="images/PZT_Stage3AndBoostSwitch.png" style="max-height:300px; width:auto;" align="left">

# In[20]:


circuit.add_resistor(name="pztR37", value="499", node1="pztn8", node2="pztn9")
circuit.add_resistor(name="pztR32", value="499", node1="pztn9", node2="pztn10")
circuit.add_library_opamp(name="pztU7", model="ad847", node1="gnd", node2="pztn10", node3="pztn12")
circuit.add_resistor(name="pztR29", value="5.6k", node1="pztn10", node2="pztn11")
circuit.add_capacitor(name="pztC35", value="6.8n", node1="pztn11", node2="pztn12")

# Boost Switch
def PZTBoost(circuit,state):
    pztRS35 = Resistor(name="pztRS35", value="0", node1="pztn11", node2="pztn12")
    if state is 'ON':
        if circuit.has_component(pztRS35):
            circuit.remove_component(pztRS35)
            print('Switched ON PZT Boost')
            
        else:
            print('PZT Boost is already ON')
    elif state is 'OFF':
        if circuit.has_component(pztRS35):
            print('PZT Boost is already OFF')
        else:
            circuit.add_component(pztRS35)
            print('Switched OFF PZT Boost')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')

#Default PZT Boost OFF
PZTBoost(circuit,'OFF')


# ### Stage 3 Notch
# <img src="images/PZT_Stage3Notch.png" style="max-height:150px; width:auto;" align="left">

# In[21]:


circuit.add_resistor(name="pztR39", value="33", node1="pztn9", node2="pztn22")
circuit.add_inductor(name="pztL2", value="470u", node1="pztn22", node2="pztn23")
circuit.add_capacitor(name="pztC47", value="910p", node1="pztn23", node2="gnd")
circuit.add_capacitor(name="pztC46", value="35p", node1="pztn23", node2="gnd")


# ### Stage 4
# <img src="images/PZT_Stage4.png" style="max-height:300px; width:auto;" align="left">

# In[22]:


circuit.add_resistor(name="pztR47", value="5.6k", node1="pztn12", node2="pztn14")
circuit.add_library_opamp(name="pztU11", model="op27", node1="gnd", node2="pztn14", node3="pztn15")
circuit.add_resistor(name="pztR40", value="5.6k", node1="pztn14", node2="pztn15")


# ### Stage 5 and Sign Switch
# <img src="images/PZT_Stage5AndSignSwitch.png" style="max-height:300px; width:auto;" align="left">

# In[23]:


circuit.add_resistor(name="pztR48", value="5.6k", node1="pztn15", node2="pztn17")
circuit.add_resistor(name="pztR43", value="5.6k", node1="pztn15", node2="pztn18")
circuit.add_library_opamp(name="pztU10", model="ad847", node1="pztn17", node2="pztn18", node3="pztn19on")
circuit.add_resistor(name="pztR41", value="5.6k", node1="pztn18", node2="pztn19on")
circuit.add_resistor(name="pztFASTMON", value="5.6k", node1="pztn19on", node2="pztnFASTMON")

# PZT Sign Switch
# Default is +
circuit.add_resistor(name="pztS1", value="0", node1="pztn15", node2="pztn17")
def PZTSign(circuit,sign):
    cmpNameList = [cmp.name for cmp in circuit.components]
    if sign is '+':
        circuit['pztS1'].node1='pztn15'
        print('Positive sign is chosen after PZT stage 5.')
    elif sign is '-':
        circuit['pztS1'].node1='gnd'
        print('Negative sign is chosen after PZT stage 5.')
    else:
        print('Error: Use sign + or -')
        print('Nothing changed.')
    circuit._regenerate_node_set()

#Default PZT Sign +
PZTSign(circuit,'+')


# ### Ramp Engage Switch
# <img src="images/PZT_RampEngage.png" style="max-height:150px; width:auto;" align="left">

# In[24]:


circuit.add_resistor(name="pztR44", value="100k", node1="pztn19off", node2="gnd")

def rampEngage(circuit,state):
    Ron1 = Resistor(name="rampEngU1ARon", value="20", node1="comn4on", node2="comn4")
    Roff1 = Resistor(name="rampEngU1Aoff", value="20", node1="comn4off", node2="comn4")
    Ron2 = Resistor(name="rampEngU1DRon", value="20", node1="pztn19on", node2="pztn19")
    Roff2 = Resistor(name="rampEngU1Doff", value="20", node1="pztn19off", node2="pztn19")
    if state is 'OFF':
        if circuit.has_component(Ron1):
            circuit.remove_component(Ron1)
            circuit.add_component(Roff1)
        if circuit.has_component(Ron2):
            circuit.remove_component(Ron2)
            circuit.add_component(Roff2)
        print('Ramp disengaged.')
    elif state is 'ON':
        if circuit.has_component(Roff1):
            circuit.remove_component(Roff1)
            circuit.add_component(Ron1)
        if circuit.has_component(Roff2):
            circuit.remove_component(Roff2)
            circuit.add_component(Ron2)
        print('Ramp engaged.')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')
        
#Default Ramp is Engaged(This is needed to be done manually once):
circuit.add_resistor(name="rampEngU1DRon", value="20", node1="pztn19on", node2="pztn19")
rampEngage(circuit,'ON')


# ### Stage 6 and Laser PZT Capacitance
# <img src="images/PZT_Stage6.png" style="max-height:150px; width:auto;" align="left">

# In[25]:


circuit.add_resistor(name="pztR46", value="15.8k", node1="pztn19", node2="pztn21")
circuit.add_capacitor(name="pztC51", value="1u", node1="pztn21", node2="gnd")
circuit.add_resistor(name="pztOUT", value="0", node1="pztn21", node2="pztnOUT")

def LaserConnected(circuit, connected=True):
    pztCLaser = Capacitor(name="pztCLaser", value="10n", node1="pztnOUT", node2="gnd")
    if connected:
        if circuit.has_component(pztCLaser):
            print('Laser PZT Capacitance already present.')
        else:
            circuit.add_component(pztCLaser)
            print('Laser PZT Capacitance Added')
    elif circuit.has_component(pztCLaser):
        circuit.remove_component(pztCLaser)
        print('Laser PZT Capacitance Removed')
    else:
        print('Lazer PZT Capacitance was not present.')

# Default: Add Lazer PZT
LaserConnected(circuit, True)


# ### Test BNC Ports and Test3 Switch
# <img src="images/PZT_TestBNCAndTest3Switch.png" style="max-height:300px; width:auto;" align="left">

# In[26]:


# OUT1
circuit.add_resistor(name="pztR45",value="1k", node1="pztn12", node2="pztnOUT1")
# OUT2
circuit.add_resistor(name="pztR42",value="1k", node1="pztn19", node2="pztnOUT2")
# EXC on PZT path, connected by switching on Test3 Switch
circuit.add_resistor(name="pztR49",value="56k", node1="pztn14", node2="pztn16")

# Input connection O Ohm resistor
circuit.add_resistor(name="pztTest3Exc", value="0", node1="test3ExcnIN", node2="pztn16on")
circuit.add_resistor(name="pztR50",value="49.9", node1="pztn16on", node2="gnd")
circuit.add_resistor(name="pztR51",value="1k", node1="pztn16off", node2="gnd")

def test3SW(circuit, state, rout1 = "50", rout2 = "50"):
    Ron = Resistor(name="Test3SWRon", value="20", node1="pztn16on", node2="pztn16")
    Roff = Resistor(name="Test3SWRoff", value="20", node1="pztn16off", node2="pztn16")
    #50 Ohm input impedance of measurement setup
    if rout1 is not "NC":
        ROUT1 = Resistor(name="pztOUT1",value=rout1, node1="pztnOUT1", node2="gnd")
    if rout2 is not "NC":
        ROUT2 = Resistor(name="pztOUT2",value=rout2, node1="pztnOUT2", node2="gnd")
    if state is 'OFF':
        if circuit.has_component(Ron):
            circuit.remove_component(Ron)
            circuit.add_component(Roff)
            if circuit.has_component(ROUT1):
                circuit.remove_component(ROUT1)
            if circuit.has_component(ROUT2):
                circuit.remove_component(ROUT2)
        else:
            print('Test3 Switch is already OFF.')
        print('Test3 EXC Not Connected.')
    elif state is 'ON':
        if circuit.has_component(Roff):
            circuit.remove_component(Roff)
            circuit.add_component(Ron)
            if rout1 is not "NC":
                circuit.add_component(ROUT1)
            if rout2 is not "NC":
                circuit.add_component(ROUT2)
        else:
            print('Test3 Switch is already ON.')
        print('Test3 EXC is connected.')
        print('Input node: test3ExcnIN\n')
        print(rout1+' Ohm and '+rout2+' Ohm impedances connected to OUT1 and OUT2 respectively.')
        print('For different input impedance of measuring instrument, change vaoues of pztOUT1 and pztOUT2')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')

# Initialize the circuit in off state (This must be done manually once)
circuit.add_resistor(name="Test3SWRoff", value="20", node1="pztn16off", node2="pztn16")
test3SW(circuit,'OFF')


# ### List of Clip Testpoints on pzt

# In[27]:


circuit.add_resistor(name="pzttp14", value="0", node1="pztn6", node2="pztntp14")
circuit.add_resistor(name="pzttp15", value="0", node1="pztn8", node2="pztntp15")
circuit.add_resistor(name="pzttp16", value="0", node1="pztn12", node2="pztntp16")
circuit.add_resistor(name="pzttp17", value="0", node1="pztn3", node2="pztntp17")
circuit.add_resistor(name="pzttp18", value="0", node1="pztn15", node2="pztntp18")
circuit.add_resistor(name="pzttp19", value="0", node1="pztn19on", node2="pztntp19")


# ***
# ## EOM Path on [Board LIGO-D040105](https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?.submit=Identifier&docid=D040105&version=)
# ### Connection from com to eom

# In[28]:


circuit.add_resistor(name="comtoeom", value="0", node1="comn9", node2="eomn1")


# ### Stage 1
# <img src="images/EOM_Stage1.png" style="max-height:350px; width:auto;" align="left">

# In[29]:


circuit.add_capacitor(name="eomC23", value="1u", node1="eomn1", node2="eomn2")
circuit.add_resistor(name="eomR22", value="1.1k", node1="eomn2", node2="eomn3")
eomR23 = createNLCompFunc(name="eomR23", node1 = "gnd", node2 = "eomn3", comType = "resistor")
eomR23(circuit,False)
circuit.add_resistor(name="eomR26", value="0", node1="gnd", node2="eomn4")
circuit.add_library_opamp(name="eomU4", model="ad797", node1="eomn4", node2="eomn3", node3="eomn5")
circuit.add_resistor(name="eomR19", value="24.9k", node1="eomn3", node2="eomn5")
eomC17 = createNLCompFunc(name="eomC17", node1 = "eomn3", node2 = "eomn5", comType = "capacitor")
eomC17(circuit,False)
circuit.add_resistor(name="eomR17", value="0", node1="eomn3", node2="eomn6")
circuit.add_capacitor(name="eomC15", value="3.3n", node1="eomn6", node2="eomn5")


# ### Stage 2
# <img src="images/EOM_Stage2.png" style="max-height:350px; width:auto;" align="left">

# In[30]:


circuit.add_capacitor(name="eomC24", value="1u", node1="eomn5", node2="eomn7")
circuit.add_resistor(name="eomR24", value="1.3k", node1="eomn7", node2="eomn8")
eomR27 = createNLCompFunc(name="eomR27", node1 = "eomn8", node2 = "eomn10", comType = "resistor")
eomR27(circuit,False)
eomC32 = createNLCompFunc(name="eomC32", node1 = "eomn10", node2 = "gnd", comType = "capacitor")
eomC32(circuit,False)
circuit.add_library_opamp(name="eomU5", model="ad797", node1="gnd", node2="eomn8", node3="eomn9")
circuit.add_resistor(name="eomR20", value="47k", node1="eomn8", node2="eomn9")
circuit.add_capacitor(name="eomC18", value="33p", node1="eomn8", node2="eomn9")
circuit.add_resistor(name="eomR18", value="1.5k", node1="eomn8", node2="eomn11")
circuit.add_capacitor(name="eomC16", value="3.3n", node1="eomn11", node2="eomn9")


# ### Stage 3
# <img src="images/EOM_Stage3.png" style="max-height:300px; width:auto;" align="left">

# In[31]:


circuit.add_capacitor(name="eomC25", value="1u", node1="eomn9", node2="eomn12")
circuit.add_resistor(name="eomR25", value="499", node1="eomn12", node2="eomn13")
eomR28 = createNLCompFunc(name="eomR28", node1 = "eomn13", node2 = "eomn15", comType = "resistor")
eomR28(circuit,False)
eomC33 = createNLCompFunc(name="eomC33", node1 = "eomn15", node2 = "gnd", comType = "capacitor")
eomC33(circuit,False)
circuit.add_library_opamp(name="eomU6", model="ad829", node1="gnd", node2="eomn13", node3="eomn14")
circuit.add_resistor(name="eomR21", value="10k", node1="eomn13", node2="eomn14")
eomC19 = createNLCompFunc(name="eomC19", node1 = "eomn13", node2 = "eomn14", comType = "capacitor")
eomC19(circuit,False)


# ### Notch Filter in parallel to Stage 1 and 2
# <img src="images/EOM_NotchFilter.png" style="max-height:150px; width:auto;" align="left">

# In[32]:


circuit.add_capacitor(name="eomC11", value="330p", node1="eomn1", node2="eomn16")
circuit.add_resistor(name="eomR14", value="3.01k", node1="eomn16", node2="eomn18")
eomC12 = createNLCompFunc(name="eomC12", node1 = "eomn1", node2 = "eomn17", comType = "capacitor")
eomC12(circuit,False)
eomR16 = createNLCompFunc(name="eomR16", node1 = "eomn17", node2 = "eomn18", comType = "resistor")
eomR16(circuit,False)
circuit.add_resistor(name="eomR15", value="100", node1="eomn18", node2="eomn13")
circuit.add_inductor(name="eomL1", value="220u", node1="eomn18", node2="eomn19")
eomC14 = createNLCompFunc(name="eomC14", node1 = "eomn19", node2 = "gnd", comType = "capacitor")
eomC14(circuit,False)
circuit.add_capacitor(name="eomC13", value="35p", node1="eomn19", node2="gnd")


# ### List of Clip Testpoints on eom

# In[33]:


circuit.add_resistor(name="eomtp11", value="0", node1="eomn5", node2="eomntp11")
circuit.add_resistor(name="eomtp12", value="0", node1="eomn9", node2="eomntp12")
circuit.add_resistor(name="eomtp13", value="0", node1="eomn14", node2="eomntp13")


# ***
# ## [HV Board LIGO-D0901893](https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?.submit=Identifier&docid=D0901893&version=)
# ### Connection from eom to HV Board

# In[34]:


circuit.add_resistor(name="eomtoeomhv", value="0", node1="eomn14", node2="eomhvn1")


# ### HV Amplifier Stage
# <img src="images/EOMHV_HVAmpStage.png" style="max-height:400px; width:auto;" align="left">

# In[35]:


circuit.add_capacitor(name="eomhvC21", value="1n", node1="eomhvn1", node2="eomhvn2")
circuit.add_capacitor(name="eomhvC22", value="220n", node1="eomhvn1", node2="eomhvn2")
circuit.add_resistor(name="eomhvR42", value="3.3k", node1="eomhvn2", node2="eomhvn3")
circuit.add_library_opamp(name="eomhvU6", model="pa98", node1="gnd", node2="eomhvn3", node3="eomhvn4")
circuit.add_resistor(name="eomhvR47", value="100k", node1="eomhvn3", node2="eomhvn4")
circuit.add_resistor(name="eomhvR45", value="1k", node1="eomhvn3", node2="eomhvn5")
circuit.add_capacitor(name="eomhvC25", value="47p", node1="eomhvn5", node2="eomhvn4")
circuit.add_resistor(name="eomhvR50", value="0", node1="eomhvn4", node2="eomhvn7")


# ### PC Mon Voltage Divider
# <img src="images/EOMHV_PCMonVoltDivider.png" style="max-height:150px; width:auto;" align="left">

# In[36]:


circuit.add_resistor(name="eomhvR51", value="100k", node1="eomhvn4", node2="eomhvn6")
circuit.add_resistor(name="eomhvR54", value="11k", node1="eomhvn6", node2="gnd")
circuit.add_resistor(name="PCMonPort", value="0", node1="eomhvn6", node2="eomnPCMon")


# ### Parallel low voltage high pass
# <img src="images/EOMHV_ParallelLVHPF.png" style="max-height:300px; width:auto;" align="left">

# In[37]:


circuit.add_capacitor(name="eomhvC23", value="100p", node1="eomhvn1", node2="eomhvn8")
circuit.add_resistor(name="eomhvR43", value="10k", node1="eomhvn8", node2="eomhvn9")
circuit.add_resistor(name="eomhvR44", value="499", node1="eomhvn9", node2="gnd")
circuit.add_library_opamp(name="eomhvU5", model="ad829", node1="gnd", node2="eomhvn9", node3="eomhvn10")
circuit.add_resistor(name="eomhvR46", value="10k", node1="eomhvn9", node2="eomhvn10")
circuit.add_resistor(name="eomhvR52", value="150", node1="eomhvn10", node2="eomhvn11")
circuit.add_resistor(name="eomhvR53", value="49.9", node1="eomhvn11", node2="gnd")
circuit.add_capacitor(name="eomhvC35", value="1n", node1="eomhvn11", node2="eomhvn7")


# ### Output Resistance and EOM Capacitance
# <img src="images/EOMHV_OutputResistor.png" style="max-height:150px; width:auto;" align="left">

# In[38]:


circuit.add_resistor(name="eomhvR55", value="100", node1="eomhvn7", node2="eomhvn12")
circuit.add_resistor(name="eomhvOUT", value="0", node1="eomhvn12", node2="eomhvnOUT")

def EOMConnected(circuit, connected=True):
    eomhvCEOM = Capacitor(name="eomhvCEOM", value="20p", node1="eomhvnOUT", node2="gnd")
    if connected:
        if circuit.has_component(eomhvCEOM):
            print('EOM Capacitance already present.')
        else:
            circuit.add_component(eomhvCEOM)
            print('EOM Capacitance Added')
    elif circuit.has_component(eomhvCEOM):
        circuit.remove_component(eomhvCEOM)
        print('EOM Capacitance Removed')
    else:
        print('EOM Capacitance was not present.')

# Default: Add EOM Capacitance.
EOMConnected(circuit, True)


# ### List of Clip Testpoints on HV Board

# In[39]:


circuit.add_resistor(name="eomhvtp1", value="0", node1="eomhvn1", node2="eomhvntp1")
circuit.add_resistor(name="eomhvtp2", value="0", node1="eomhvn10", node2="eomhvntp2")
circuit.add_resistor(name="eomhvtp3", value="0", node1="eomhvn4", node2="eomhvntp3")
circuit.add_resistor(name="eomhvtp4", value="0", node1="eomhvn6", node2="eomhvntp4")


# # Create a script from this notebook
# The cell below this will not be present in script to avoid unnecessary overwrites when running the script.

