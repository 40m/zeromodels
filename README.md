# Zero Models

This repo contains zero models of some important circuit boards which can be imported for calculations and/or do modifications. Each directory would be one such circuit representation. Any switches, jumpers, or special options like variable gain sould be present throguh importable functions giving users plug-and-play experience with the imported circuit objects.

## Installing zero
Install [zero](https://git.ligo.org/sean-leavey/zero) and upgrade it to latest version:
```
pip install zero
pip install zero --upgrade
```
