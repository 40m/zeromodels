#!/usr/bin/env python
# coding: utf-8

# In[1]:


from zero import Circuit
from zero.components import Resistor, OpAmp


# In[4]:


AD822 = {'model': 'AD822',
         'a0': 3e5,        # open loop gain at 5 kOhm load
         'gbw': 1.8e6,
         'vnoise': '13n',
         'vcorner': 20,
         'inoise': '0.8f', # At 1 kHz
         'icorner': 20,   # Guess
         'vmax': 13.2,
         'imax': 70e-3,
         'sr': 3e6}


# In[9]:


SAB = Circuit()
SAB.add_resistor(value=100, name='R25', node1='PDA', node2='n1')
SAB.add_component(OpAmp(node1='gnd', node2='n1', node3='n2', name='U3A', **AD822))
SAB.add_resistor(value=121e3, name='R18', node1='n1', node2='n2')
SAB.add_capacitor(value=0.001e-6, name='C10', node1='n1', node2='n2')


# In[10]:


SAB.add_resistor(value=19.6e3, name='R24', node1='n2', node2='n3')
SAB.add_component(OpAmp(node1='gnd', node2='n3', node3='n5', name='U3B', **AD822))
SAB.add_resistor(value=787, name='R23', node1='n2', node2='n4')
SAB.add_capacitor(value=10e-6, name='C13', node1='n4', node2='n3')
SAB.add_capacitor(value=10e-6, name='C16', node1='n4', node2='n3')
SAB.add_resistor(value=19.6e3, name='R20', node1='n3', node2='n5')


# In[ ]:


SAB.add_resistor(value=4.99e3, name='R16', node1='n5', node2='n6')
SAB.add_library_opamp(model='AD8672', name='U2A', node1='n5', node2='n6', node3='n7')
SAB.add_resistor(value=4.99e3, name='R15', node1='n6', node2='n7')

SAB.add_resistor(value=4.99e3, name='R21', node1='n5', node2='n8')
SAB.add_library_opamp(model='AD8672', name='U2B', node1='gnd', node2='n8', node3='n9')
SAB.add_resistor(value=4.99e3, name='R19', node1='n8', node2='n9')


# In[ ]:


SAB.add_resistor(value=0, name='R17', node1='n7', node2='PDOutP')
SAB.add_resistor(value=0, name='R22', node1='n9', node2='PDOutN')


