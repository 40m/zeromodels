# HAM-A Coil Drivers

### Zero representation of LIGO HAM-A Coil Drivers

## Purpose
To have a single import point for creating a circuit object which represents
Satellite Amplifier circuit board
[LIGO-D080276](https://dcc.ligo.org/LIGO-D080276-v2).
The code is written in jupyter notebook to easily understand the flow of code
and which element (component and nodes) is which. The notebook in the end
writes a script copy of itself which we can import out of this module.

## Documentation
Following are some useful node/component names to remember:

|	Node name	|	Function/Parameter	|
|	---	|	---	|
| PDA | Photodiode input current node.|
| PDOutN | Transamplified output positive signal node.|
| PDOutN | Transamplified output negative signal node.|

## Contribution
Everyone's contribution towards any bug fixes and additional features are most
welcome but please make your changes in the notebook, save it and use 'restart
and run all' function to generate equivalent script copy. This will ensure that
the notebook and the script do not diverge from each other and we always have
an up-to-date notebook environment to understand the circuit. Thanks!

## Email
Anchal Gupta - anchal@caltech.edu
