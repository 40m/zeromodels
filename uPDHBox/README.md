# HAM-A Coil Drivers

### Zero representation of LIGO HAM-A Coil Drivers

## Purpose
To have a single import point for creating a circuit object which represents
Satellite Amplifier circuit board
[LIGO-D0901351](https://dcc.ligo.org/LIGO-D0901351-v1).
The code is written in jupyter notebook to easily understand the flow of code
and which element (component and nodes) is which. The notebook in the end
writes a script copy of itself which we can import out of this module.

## Documentation
All yellow marked signal ports in the schematic are named as it is in the
zero model without any spaces.
Following are some useful node names to remember:

|	Node name	|	Function/Parameter	|
|	---	|	---	|
| ServoIn | Servo Input.|
| InMon | Input monitor.|
| OutMon | Output Monitor.|
| TP# | Test points in the main signal chain. Eg. TP4.|
| J3A | Servo Input BNC port.|
| J1A | Piezo BNC port.|

The model provides options of setting the variable gain, switching Boost,
and inverter switch.

|	Function	|	What does it do?	|	Arguments and usage	|
|	---	|	---	|	---	|
|	VarGain(circuit, Rpot=None, G=None, Vg=None, vb=True, R27=1e3, R26=1e3)	|	Set variable gain	|	circuit: Circuit object <br>Rpot:R7 potentiometer setting in Ohms (0-10kOhms)<br>Vg:Gain setting voltage<br>vb: verbose<br><br>R27, R26: Resitors associated to preamplifier again in AD8336.<br>One only needs to provide one of arguments Rpot, G, or Vg.	|
|	Boost(circuit,state)	|	Switch on or off Boost. |	circuit: Circuit object <br>State: 'ON' or 'OFF'	|
|	Inverter(circuit,state)	|	Switch on or off inversiont. |	circuit: Circuit object <br>State: 'ON' or 'OFF'	|

## Contribution
Everyone's contribution towards any bug fixes and additional features are most
welcome but please make your changes in the notebook, save it and use 'restart
and run all' function to generate equivalent script copy. This will ensure that
the notebook and the script do not diverge from each other and we always have
an up-to-date notebook environment to understand the circuit. Thanks!

## Email
Anchal Gupta - anchal@caltech.edu
