#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from zero import Circuit
from zero.components import Resistor, OpAmp
from math import log10


# In[ ]:


# Variable Gain Amplifier. Will be used by changing a0.
AD8336 = {'model': 'AD8336',
          'a0': 10,      # Nominal 20 dB gain. Will be changed.   
          'gbw': 80e6,
          'vnoise': '3n',
          'vcorner': 1e3,
          'inoise': '3p',
          'icorner': 1e3, # Guess
          'vmax': 10,
          'imax': 72e-3,
          'sr': 550e6}


# ## Insertion Amplifier Stage

# In[ ]:


uPDH = Circuit()
uPDH.add_resistor(value=0, name='J3A', node1='ServoIn', node2='J3A')
uPDH.add_resistor(value=0, name='TP1', node1='ServoIn', node2='TP1')
uPDH.add_library_opamp(model='OP27', name='U1', node1='ServoIn', node2='n1', node3='InMon')
uPDH.add_resistor(value=49.9, name='R17', node1='ServoIn', node2='gnd')
uPDH.add_resistor(value=1e3, name='R14', node1='n1', node2='gnd')
uPDH.add_resistor(value=1e3, name='R12', node1='n1', node2='InMon')
uPDH.add_resistor(value=0, name='TP3', node1='InMon', node2='TP3')


# ## Variable Gain Stage

# In[ ]:


uPDH.add_component(OpAmp(node1='InMon', node2='gnd',
                         node3='n2', name='U6', **AD8336))
def VarGain(circuit, Rpot=None, G=None, Vg=None, vb=True,
            R27=1e3, R26=1e3):
    preAmpGain = 20 * log10(1 + R26/R27)
    if Rpot is None:
        if G is None:
            if Vg is None:
                print('Error: Provide gain in dB or gain setting voltage in V.')
                print('Nothing changed.')
                return 0
            else:
                G = 50 * Vg + 4.4 + preAmpGain
    else:
        G = -11.19375 + Rpot * 3.11875e-3 + preAmpGain
    if G>30.0 or G<-10.0:
        print('Error: Gain can be set between -10dB to 30 dB only.')
        print('Nothing changed.')
        return 0
    circuit["U6"].a0 = 10**(G/20) #Gain in absolute value
    if vb:
        print('Variable Gain set to '+str(round(G,2))+' dB.')
# Default potentiometer set to 6.6kOhms
VarGain(uPDH, Rpot=6.6e3)


# ## Optional Transfer Function Stage

# In[ ]:


uPDH.add_resistor(value=0, name='TP13', node1='n2', node2='TP13')
uPDH.add_resistor(value=1e3, name='R29', node1='n2', node2='n4')
uPDH.add_resistor(value=1e3, name='R32', node1='n3', node2='gnd')
uPDH.add_library_opamp(model='OP27', name='U7', node1='n3', node2='n4', node3='n5', )
uPDH.add_resistor(value=1e3, name='R30', node1='n4', node2='n5')
uPDH.add_resistor(value=10, name='R31', node1='n4', node2='n6')
uPDH.add_capacitor(value=3.3e-6, name='C28', node1='n6', node2='n5')
uPDH.add_capacitor(value=3.3e-6, name='C18', node1='n7', node2='n4')
uPDH.add_resistor(value=0, name='TP16', node1='n5', node2='TP16')


# ## Integrator Stage

# In[ ]:


uPDH.add_resistor(value=1e3, name='R23', node1='n5', node2='n9')
uPDH.add_resistor(value=1e3, name='R20', node1='n8', node2='gnd')
uPDH.add_library_opamp(model='LF356', name='U4', node1='n8', node2='n9', node3='n10')
uPDH.add_resistor(value=10, name='R24', node1='n9', node2='n12')
uPDH.add_capacitor(value=3.3e-6, name='C6', node1='n12', node2='n10')
uPDH.add_resistor(value=10e3, name='R16', node1='n11', node2='n10')
uPDH.add_capacitor(value=3.3e-6, name='C15', node1='n13', node2='n9')
uPDH.add_resistor(value=0, name='TP10', node1='n10', node2='TP10')


# ### Boost Switch

# In[ ]:


def Boost(circuit, state):
    boost = Resistor(name="boost", value="0", node1="n9", node2="n11")
    if state.upper() == 'ON':
        if circuit.has_component(boost):
            circuit.remove_component(boost)
            print('Switched ON Boost')
        else:
            print('Boost is already ON')
    elif state.upper() == 'OFF':
        if circuit.has_component(boost):
            print('Boost is already OFF')
        else:
            circuit.add_component(boost)
            print('Switched OFF Boost')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')

# Default Boost ON
Boost(uPDH,'ON')


# ## Inverter and Piezo Driver Stage

# In[ ]:


uPDH.add_resistor(value=0, name='TP2', node1='n10', node2='TP2')
uPDH.add_resistor(value=10e3, name='R19', node1='n10', node2='n15')
uPDH.add_resistor(value=10e3, name='R21', node1='n10', node2='n14')
uPDH.add_library_opamp(model='OP27', name='U3', node1='n14', node2='n15', node3='n16')
uPDH.add_resistor(value=10e3, name='R15', node1='n15', node2='n16')
uPDH.add_resistor(value=0, name='TP4', node1='n16', node2='TP4')


# ### Inverter Switch

# In[ ]:


def Inverter(circuit,state):
    invert = Resistor(name="invert", value="0", node1="n14", node2="gnd")
    if state.upper() == 'OFF':
        if circuit.has_component(invert):
            circuit.remove_component(invert)
            print('Switched OFF inversion')
        else:
            print('Inversion is already OFF')
    elif state.upper() == 'ON':
        if circuit.has_component(invert):
            print('Inversion is already ON')
        else:
            circuit.add_component(invert)
            print('Switched ON inversion')
    else:
        print('Error: Use options ON or OFF')
        print('Nothing changed.')

# Default invert ON
Inverter(uPDH,'ON')


# In[ ]:


uPDH.add_resistor(value=10, name='R11', node1='n16', node2='OutMon')
uPDH.add_resistor(value=0, name='J1A', node1='OutMon', node2='J1A')


