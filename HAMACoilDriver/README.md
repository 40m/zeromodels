# HAM-A Coil Drivers

### Zero representation of LIGO HAM-A Coil Drivers

## Purpose
To have a single import point for creating a circuit object which represents
HAM-A Coil Driver circuit board [LIGO-D1100117](https://dcc.ligo.org/LIGO-D1100117-v3).
The code is written in jupyter notebook to easily understand the flow of code
and which element (component and nodes) is which. The notebook in the end
writes a script copy of itself which we can import out of this module.

## Example
Checkout ExampleUse.ipynb to see how to use this module.

## Documentation
Following are some useful node/component names to remember:

|	Node name	|	Function/Parameter	|
|	---	|	---	|
| VinP | Positive input signal node.|
| VinN | Negative input signal node.|
| VmP | Positive output monitor node.|
| VmN | Negative output monitor node.|
| CoilP | Positive node connection to coil.|
| CoilN | Negative node connection to coil.|

Additional functions:

|	Function	|	What does it do?	|	Arguments and usage	|
|	---	|	---	|	---	|
| W1| Short jumper W1 on board.|circuit: Circuit object <br>status: 'ON' or 'OFF'|
| W2| Short jumper W2 on board.|circuit: Circuit object <br>status: 'ON' or 'OFF'|
| setMode| Set Acquisition/Run mode.|circuit: Circuit object <br>mode: 'Acq' or 'Run'|

## Contribution
Everyone's contribution towards any bug fixes and additional features are most
welcome but please make your changes in the notebook, save it and use 'restart
and run all' function to generate equivalent script copy. This will ensure that
the notebook and the script do not diverge from each other and we always have
an up-to-date notebook environment to understand the circuit. Thanks!

## Email
Anchal Gupta - anchal@caltech.edu
