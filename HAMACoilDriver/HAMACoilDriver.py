#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from zero import Circuit
from zero.components import Resistor, OpAmp


# In[ ]:


opa544 = {'model': 'OPA544',
          'a0': 141253.8,
          'gbw': 2e6,
          'vnoise': '36n',
          'vcorner': 7,
          'inoise': '3f',
          'icorner': 7,
          'vmax': 13.6,
          'imax': 4,
          'sr': 9e6}


# In[ ]:


HAMA = Circuit()
HAMA.add_resistor(value=10e3, name='R16', node1='VinP', node2='n1')
HAMA.add_resistor(value=10e3, name='R22', node1='VinN', node2='n1')
HAMA.add_resistor(value=1e6, name='R20', node1='gnd', node2='nW1')
def W1(cir, state='OFF'):
    W1 = Resistor(value=0, name='W1', node1='nW1', node2='n1')
    if state.upper() == 'ON' and W1 not in cir:
        cir.add_component(W1)
        print('Added', W1)
    elif state.upper() == 'OFF' and W1 in cir:
        cir.remove_component(W1)
        print('Removed', W1)
def W2(cir, state='OFF'):
    W2 = Resistor(value=0, name='W2', node1='gnd', node2='n1')
    if state.upper() == 'ON' and W2 not in cir:
        cir.add_component(W2)
        print('Added', W2)
    elif state.upper() == 'OFF' and W2 in cir:
        cir.remove_component(W2)
        print('Removed', W2)
W2(HAMA, 'ON')


# In[ ]:


HAMA.add_library_opamp(model='AD8672', name='IC4A', node1='VinP', node2='n2', node3='n4')
HAMA.add_resistor(value=499, name='R9', node1='n2', node2='n4')
HAMA.add_library_opamp(model='AD8672', name='IC4B', node1='VinN', node2='n3', node3='n5')
HAMA.add_resistor(value=499, name='R21', node1='n3', node2='n5')


# In[ ]:


HAMA.add_resistor(value=16.2e3, name='R14', node1='n4', node2='n8')
HAMA.add_resistor(value=160, name='R13', node1='n4', node2='n6')
HAMA.add_capacitor(value=0.47e-6, name='C11', node1='n6', node2='n8')

HAMA.add_resistor(value=16.2e3, name='R25', node1='n5', node2='n9')
HAMA.add_resistor(value=160, name='R27', node1='n5', node2='n7')
HAMA.add_capacitor(value=0.47e-6, name='C21', node1='n7', node2='n9')

HAMA.add_capacitor(value=4.7e-6, name='C19', node1='n10', node2='n9')
HAMA.add_resistor(value=1e6, name='R18', node1='n8', node2='n10')
HAMA.add_resistor(value=3.32e3, name='R15', node1='n8', node2='nSwitch')


# In[ ]:


def setMode(cir, mode='Acq'):
    SW = Resistor(value=0, name='SW', node1='nSwitch', node2='n10')
    if mode.lower() == 'acq':
        if SW in cir:
            cir.remove_component(SW)
    elif mode.lower() == 'run':
        if SW not in cir:
            cir.add_component(SW)
setMode(HAMA, 'Acq')


# In[ ]:


HAMA.add_library_opamp(model='AD8671', name='IC2', node1='n8', node2='n11', node3='n13')
HAMA.add_resistor(value=1e3, name='R7', node1='n11', node2='gnd')
HAMA.add_resistor(value=1.21e3, name='R5', node1='n11', node2='VmP')
HAMA.add_capacitor(value=150e-12, name='C6', node1='n11', node2='n13')
HAMA.add_resistor(value=1e3, name='R10', node1='n13', node2='n15')
HAMA.add_resistor(value=1e3, name='R12', node1='n15', node2='gnd')
HAMA.add_component(OpAmp(node1='n15', node2='n17', node3='VmP', name='IC3', **opa544))
HAMA.add_resistor(value=1e3, name='R6', node1='gnd', node2='n17')
HAMA.add_resistor(value=1e3, name='R8', node1='n17', node2='VmP')

HAMA.add_library_opamp(model='AD8671', name='IC6', node1='n9', node2='n12', node3='n14')
HAMA.add_resistor(value=1e3, name='R24', node1='n12', node2='gnd')
HAMA.add_resistor(value=1.21e3, name='R23', node1='n12', node2='VmN')
HAMA.add_capacitor(value=150e-12, name='C20', node1='n12', node2='n14')
HAMA.add_resistor(value=1e3, name='R30', node1='n14', node2='n16')
HAMA.add_resistor(value=1e3, name='R34', node1='n16', node2='gnd')
HAMA.add_component(OpAmp(node1='n16', node2='n18', node3='VmN', name='IC7', **opa544))
HAMA.add_resistor(value=1e3, name='R26', node1='gnd', node2='n18')
HAMA.add_resistor(value=1e3, name='R28', node1='n18', node2='VmN')


# In[ ]:


HAMA.add_resistor(value=1.2e3, name='R11', node1='VmP', node2='CoilP')
HAMA.add_resistor(value=1.2e3, name='R33', node1='VmN', node2='CoilN')


